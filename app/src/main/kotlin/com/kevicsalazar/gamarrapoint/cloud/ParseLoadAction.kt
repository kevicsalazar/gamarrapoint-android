package com.kevicsalazar.gamarrapoint.cloud

import com.kevicsalazar.gamarrapoint.R;
import com.kevicsalazar.appkit.enums.LoadStatus;
import com.kevicsalazar.appkit.enums.UpdateTime;
import com.kevicsalazar.appkit.callbacks.LoadCallback;
import com.kevicsalazar.appkit.utils.Clock;
import com.kevicsalazar.appkit.utils.ParserUtils;
import com.kevicsalazar.gamarrapoint.storage.PreferenceProvider;
import com.kevicsalazar.gamarrapoint.utils.NetworkService;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@Suppress("unchecked_cast")
class ParseLoadAction(private val pref: PreferenceProvider, private val net: NetworkService) {

    fun <C : ParseObject, L> findSomething(query: ParseQuery<C>, cb: LoadCallback<L>) {
        query.findInBackground { list, e1 ->
            cb.onLoadStatus(LoadStatus.LOADED)
            if (e1 == null) {
                cb.onLoadSuccess(ParserUtils.parse<C>(list) as L)
            } else {
                cb.onLoadFailure(getMsg(e1))
            }
        }
    }

    fun <C : ParseObject, L> findSomething(query: ParseQuery<C>, time: UpdateTime, tag: String, cb: LoadCallback<L>) {
        if (UpdateTime.isTimeToUpdate(pref.getLong(tag, 0), time)) {
            if (net.isOnline) {
                cb.onLoadStatus(LoadStatus.LOADING)
                query.findInBackground { newList, e1 ->
                    cb.onLoadStatus(LoadStatus.LOADED);
                    if (e1 == null) {
                        query.fromLocalDatastore().findInBackground { localList, e2 ->
                            if (e2 == null) {
                                ParseObject.unpinAllInBackground (localList, { e3 ->
                                    if (e3 == null) {
                                        ParseObject.pinAllInBackground (newList, { e4 ->
                                            if (e4 == null) {
                                                pref.putLong(tag, Clock.REAL.millis());
                                                cb.onLoadSuccess(ParserUtils.parse<C>(newList) as L)
                                            }
                                        })
                                    }
                                })
                            }
                        }
                    } else {
                        cb.onLoadFailure(getMsg(e1))
                    }
                }
            } else {
                cb.onLoadFailure(R.string.error_network)
            }
        } else {
            findSomething(query.fromLocalDatastore(), cb)
        }
    }

    private fun getMsg(e: ParseException): Int {
        e.printStackTrace()
        when (e.code) {
            ParseException.CONNECTION_FAILED -> return R.string.error_network
            else -> return R.string.error_generic
        }
    }

}