package com.kevicsalazar.gamarrapoint.cloud.di

import com.kevicsalazar.gamarrapoint.storage.PreferenceProvider
import com.kevicsalazar.gamarrapoint.utils.NetworkService
import com.kevicsalazar.gamarrapoint.cloud.ParseLoadAction
import dagger.Module
import dagger.Provides


/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@Module
class ParseModule {

    @Provides fun provideParseLoadAction(pref: PreferenceProvider, net: NetworkService): ParseLoadAction {
        return ParseLoadAction(pref, net)
    }

}