package com.kevicsalazar.gamarrapoint

import com.kevicsalazar.appkit.scopes.PerApp
import com.kevicsalazar.gamarrapoint.cloud.ParseLoadAction
import com.kevicsalazar.gamarrapoint.cloud.di.ParseModule
import com.kevicsalazar.gamarrapoint.storage.PreferenceProvider
import com.kevicsalazar.gamarrapoint.storage.di.StorageModule
import com.kevicsalazar.gamarrapoint.utils.AnalyticsProvider
import com.kevicsalazar.gamarrapoint.utils.NetworkService
import com.kevicsalazar.gamarrapoint.utils.ResourcesProvider
import com.kevicsalazar.gamarrapoint.utils.di.PlayServiceModule

import dagger.Component

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@PerApp
@Component(modules = arrayOf(AppModule::class, ParseModule::class, StorageModule::class, PlayServiceModule::class))
public interface AppComponent {

    fun inject(app: App)

    fun getNetworkService(): NetworkService

    fun getResourcesProvider(): ResourcesProvider

    fun getParseLoadAction(): ParseLoadAction

    fun getPreferenceProvider(): PreferenceProvider

    fun getAnalyticsProvider(): AnalyticsProvider

    object Initializer {
        fun init(app: App): AppComponent =
                DaggerAppComponent.builder()
                        .appModule(AppModule(app))
                        .parseModule(ParseModule())
                        .storageModule(StorageModule(app))
                        .playServiceModule(PlayServiceModule(app))
                        .build()
    }

}