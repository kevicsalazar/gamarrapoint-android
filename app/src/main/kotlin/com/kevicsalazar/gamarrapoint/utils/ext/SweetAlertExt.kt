package com.kevicsalazar.gamarrapoint.utils.ext

import android.content.Context
import android.support.v4.content.ContextCompat
import cn.pedant.SweetAlert.SweetAlertDialog
import com.kevicsalazar.gamarrapoint.R

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
fun Context.getSweetProgress(text: String, color: Int = ContextCompat.getColor(this, R.color.color_accent)): SweetAlertDialog {
    var pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
    pDialog.progressHelper?.barColor = color
    pDialog.setCancelable(false)
    pDialog.setTitleText(text)
    return pDialog
}