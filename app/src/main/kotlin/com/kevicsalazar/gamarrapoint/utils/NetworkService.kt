package com.kevicsalazar.gamarrapoint.utils

import android.content.Context
import android.net.ConnectivityManager
import com.kevicsalazar.gamarrapoint.App

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class NetworkService(private val app: App) {

    val isOnline: Boolean
        get() {
            val connMgr = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connMgr.activeNetworkInfo
            return (networkInfo != null && networkInfo.isConnected)
        }

}
