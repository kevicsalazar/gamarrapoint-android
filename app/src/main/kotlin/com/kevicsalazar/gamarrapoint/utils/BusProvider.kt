package com.kevicsalazar.gamarrapoint.utils

import com.squareup.otto.Bus

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
object BusProvider {

    internal val bus = Bus()

    fun post(event: Any) {
        bus.post(event)
    }

    fun register(event: Any) {
        bus.register(event);
    }

    fun unregister(event: Any) {
        bus.unregister(event);
    }

}