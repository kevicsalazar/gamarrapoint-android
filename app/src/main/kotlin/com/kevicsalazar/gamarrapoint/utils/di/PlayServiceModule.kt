package com.kevicsalazar.gamarrapoint.utils.di

import com.kevicsalazar.gamarrapoint.App
import com.kevicsalazar.appkit.scopes.PerApp
import com.kevicsalazar.gamarrapoint.utils.AnalyticsProvider
import dagger.Module
import dagger.Provides

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@Module
class PlayServiceModule(private val app: App) {

    @Provides @PerApp fun provideAnalyticsProvider(): AnalyticsProvider {
        return AnalyticsProvider(app)
    }

}