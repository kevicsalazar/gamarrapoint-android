package com.kevicsalazar.gamarrapoint.utils

import com.google.android.gms.analytics.GoogleAnalytics
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker
import com.kevicsalazar.gamarrapoint.App
import com.kevicsalazar.gamarrapoint.BuildConfig

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class AnalyticsProvider(private val app: App) {

    private val tracker: Tracker

    init {
        var analytics = GoogleAnalytics.getInstance(app);
        analytics.setLocalDispatchPeriod(1800);
        tracker = analytics.newTracker(BuildConfig.GOOGLE_TRACKING_ID);
        tracker.enableAdvertisingIdCollection(true);
    }

    fun signUp(code: String) {
        tracker.set("&uid", code);
    }

    fun trackScreenName(name: String) {
        tracker.setScreenName(name);
        tracker.send(HitBuilders.ScreenViewBuilder().build());
    }

    fun trackEvent(category: String, action: String, label: String) {
        tracker.send(HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());
    }

}