package com.kevicsalazar.gamarrapoint.utils

import android.content.res.ColorStateList
import android.support.v4.content.ContextCompat
import com.kevicsalazar.gamarrapoint.App

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class ResourcesProvider(private val app: App) {

    fun getString(resId: Int): String {
        return app.resources.getString(resId)
    }

    fun getColor(resId: Int): Int {
        return ContextCompat.getColor(app, resId)
    }

    fun getColorStateList(resId: Int): ColorStateList {
        return ContextCompat.getColorStateList(app, resId)
    }

}
