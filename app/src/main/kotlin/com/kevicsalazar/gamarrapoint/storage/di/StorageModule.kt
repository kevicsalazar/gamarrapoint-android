package com.kevicsalazar.gamarrapoint.storage.di

import com.kevicsalazar.gamarrapoint.App
import com.kevicsalazar.appkit.scopes.PerApp
import com.kevicsalazar.gamarrapoint.storage.PreferenceProvider
import dagger.Module
import dagger.Provides

/**
 * Created by Kevin Salazar
 */
@Module
class StorageModule(private val app: App) {

    @Provides @PerApp fun providePreferenceProvider(): PreferenceProvider = PreferenceProvider(app)

}