package com.kevicsalazar.gamarrapoint.storage

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.kevicsalazar.gamarrapoint.App

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class PreferenceProvider(app: App) {

    private val preferences: SharedPreferences
    private val editor: SharedPreferences.Editor;

    init {
        preferences = PreferenceManager.getDefaultSharedPreferences(app)
        editor = preferences.edit()
    }

    fun getBoolean(key: String): Boolean {
        return preferences.getBoolean(key, false)
    }

    fun getBoolean(key: String, defValue: Boolean): Boolean {
        return preferences.getBoolean(key, defValue)
    }

    fun putBoolean(key: String, value: Boolean) {
        editor.putBoolean(key, value).apply()
    }

    fun getString(key: String): String {
        return preferences.getString(key, null)
    }

    fun getString(key: String, defValue: String): String {
        return preferences.getString(key, defValue)
    }

    fun putString(key: String, value: String) {
        editor.putString(key, value).apply()
    }

    fun getInt(key: String): Int {
        return preferences.getInt(key, 0)
    }

    fun getInt(key: String, defValue: Int): Int {
        return preferences.getInt(key, defValue)
    }

    fun putInt(key: String, value: Int) {
        editor.putInt(key, value).apply()
    }

    fun getLong(key: String): Long {
        return preferences.getLong(key, 0)
    }

    fun getLong(key: String, defValue: Long): Long {
        return preferences.getLong(key, defValue)
    }

    fun putLong(key: String, value: Long) {
        editor.putLong(key, value).apply()
    }

    fun remove(key: String) {
        editor.remove(key).apply()
    }

    fun clear() {
        editor.clear().apply()
    }

}
