package com.kevicsalazar.gamarrapoint.ui.mvp.model

import com.parse.ParseClassName
import com.parse.ParseFile
import com.parse.ParseObject

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@ParseClassName(ParseModel.CLIENT)
class Client : ParseObject() {

    val name: String
        get() = getString("name") ?: ""

    val description: String
        get() = getString("description") ?: ""

    val ruc: String
        get() = getString("ruc") ?: ""

    val logo: ParseFile?
        get() = getParseFile("logo")

    val email: String
        get() = getString("email") ?: ""

    val phoneList: List<String>
        get() = getList("phoneList")

    val enable: Boolean
        get() = getBoolean("enable")

}