package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.os.Bundle
import android.view.WindowManager
import com.kevicsalazar.appkit.BaseActivity
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.appkit.model.GuideModel
import com.kevicsalazar.appkit.views.PageIndicator
import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.adapters.SimplePagerAdapter
import kotlinx.android.synthetic.activity_userguide.*

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class UserGuideActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        val adapter = SimplePagerAdapter(supportFragmentManager)
        adapter.addFragment(getUserGuidePage(0))
        adapter.addFragment(getUserGuidePage(1))
        adapter.addFragment(getUserGuidePage(2))
        adapter.addFragment(getUserGuidePage(3))
        adapter.addFragment(getUserGuidePage(4))
        viewpager.adapter = adapter

        indicator.setIndicatorType(PageIndicator.IndicatorType.CIRCLE);
        indicator.solidColor
        indicator.setViewPager(viewpager)
    }

    override val layout: Int = R.layout.activity_userguide

    override val presenter: BasePresenter? get() = null

    override fun setupComponent() {

    }

    fun getUserGuidePage(position: Int): UserGuideFragment {
        var guideModel: GuideModel
        when (position) {
            0    -> guideModel = GuideModel(R.string.gt_01, R.string.gd_01, R.color.color_primary, R.mipmap.ss_01)
            1    -> guideModel = GuideModel(R.string.gt_02, R.string.gd_02, R.color.color_accent, R.mipmap.ss_02)
            2    -> guideModel = GuideModel(R.string.gt_03, R.string.gd_03, R.color.color_primary, R.mipmap.ss_03)
            3    -> guideModel = GuideModel(R.string.gt_04, R.string.gd_04, R.color.color_accent, R.mipmap.ss_04)
            else -> guideModel = GuideModel(R.string.gt_05, R.string.gd_05, R.color.color_primary, R.mipmap.ss_05)
        }
        return UserGuideFragment.show(guideModel)
    }

}