package com.kevicsalazar.gamarrapoint.ui.mvp.presenters

import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.appkit.callbacks.LoadCallback
import com.kevicsalazar.gamarrapoint.cloud.ParseLoadAction
import com.kevicsalazar.gamarrapoint.ui.mvp.model.*
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.events.ToastMessage
import com.kevicsalazar.gamarrapoint.utils.BusProvider
import com.parse.ParseQuery

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class ShopsPresenter(var parse: ParseLoadAction) : BasePresenter {

    var view: View? = null

    fun loadCategoryList() {
        val query = ParseQuery.getQuery(Category::class.java)
        query.orderByAscending("order")
        query.fromLocalDatastore()
        parse.findSomething(query,
                LoadCallback<List<Category>>({

                }, {
                    view?.addItemsLikeTabs(it)
                }, {
                    BusProvider.post(ToastMessage(it))
                }))
    }

    override fun onStart() {

    }

    override fun onStop() {

    }

    interface View {

        fun addItemsLikeTabs(list: List<Category>);

    }

}