package com.kevicsalazar.gamarrapoint.ui.di

import com.kevicsalazar.gamarrapoint.cloud.ParseLoadAction
import com.kevicsalazar.gamarrapoint.storage.PreferenceProvider
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.*
import com.kevicsalazar.gamarrapoint.utils.AnalyticsProvider
import com.kevicsalazar.gamarrapoint.utils.NetworkService
import dagger.Module
import dagger.Provides

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@Module
public class ActivityModule {

    @Provides fun provideLoginPresenter(analytics: AnalyticsProvider, pref: PreferenceProvider): LoginPresenter = LoginPresenter(analytics, pref)

    @Provides fun provideMainPresenter(status: NetworkService, pref: PreferenceProvider): MainPresenter = MainPresenter(status, pref)

    @Provides fun provideHomePresenter(parse: ParseLoadAction): HomePresenter = HomePresenter(parse)

    @Provides fun provideShopsPresenter(parse: ParseLoadAction): ShopsPresenter = ShopsPresenter(parse)

    @Provides fun provideClientListPresenter(parse: ParseLoadAction): ClientListPresenter = ClientListPresenter(parse)

    @Provides fun provideProductDetailPresenter(): ProductDetailPresenter = ProductDetailPresenter()

    @Provides fun provideShopProfilePresenter(): ShopProfilePresenter = ShopProfilePresenter()

    @Provides fun provideProductListPresenter(): ProductListPresenter = ProductListPresenter()

}
