package com.kevicsalazar.gamarrapoint.ui.mvp.presenters

import android.app.Activity
import android.os.Bundle
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.appkit.utils.ext.e
import com.kevicsalazar.gamarrapoint.storage.PreferenceProvider
import com.kevicsalazar.gamarrapoint.utils.AnalyticsProvider
import com.parse.ParseFacebookUtils

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class LoginPresenter(val analytics: AnalyticsProvider, val pref: PreferenceProvider) : BasePresenter {

    var view: View? = null

    fun loginWithFacebook(activity: Activity) {
        ParseFacebookUtils.logInWithReadPermissionsInBackground(activity, arrayListOf("public_profile", "email"),
                { parseUser, e1 ->
                    if (e1 == null) {
                        if (parseUser != null) {
                            val request = GraphRequest.newMeRequest(
                                    AccessToken.getCurrentAccessToken()
                            ) { data, response1 ->
                                if (data != null) {
                                    analytics.signUp(data.getString("id"))
                                    pref.putString("facebookUserId", data.getString("id"))
                                    pref.putString("facebookUserName", data.getString("name"))
                                    pref.putString("facebookUserEmail", data.getString("email"))
                                    view?.goToMainActivity()
                                }
                            }
                            val parameters = Bundle()
                            parameters.putString("fields", "id, name, email")
                            request.parameters = parameters
                            request.executeAsync()
                        } else {
                            e("MyApp", "Uh oh. The user cancelled the Facebook login.")
                        }
                    } else {
                        e1.printStackTrace()
                    }
                })
    }

    override fun onStart() {

    }

    override fun onStop() {

    }

    interface View {

        fun goToMainActivity()

    }

}