package com.kevicsalazar.gamarrapoint.ui.mvp.presenters.events

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class ToastMessage(val id: Int)