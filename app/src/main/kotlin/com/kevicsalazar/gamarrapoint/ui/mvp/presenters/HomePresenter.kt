package com.kevicsalazar.gamarrapoint.ui.mvp.presenters

import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.appkit.callbacks.LoadCallback
import com.kevicsalazar.appkit.enums.LoadStatus
import com.kevicsalazar.appkit.enums.UpdateTime
import com.kevicsalazar.gamarrapoint.cloud.ParseLoadAction
import com.kevicsalazar.gamarrapoint.ui.mvp.model.*
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.events.ToastMessage
import com.kevicsalazar.gamarrapoint.utils.BusProvider
import com.parse.ParseQuery

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class HomePresenter(val parse: ParseLoadAction) : BasePresenter {

    var view: View? = null

    fun loadData() {
        loadCategoryClientList();
    }

    private fun loadCategoryClientList() {
        val query = ParseQuery.getQuery(CategoryClient::class.java)
        parse.findSomething(query, UpdateTime.AUTOLOW, ParseModel.CATEGORY_CLIENT,
                LoadCallback<List<CategoryClient>>({
                    if (it == LoadStatus.LOADING) view?.progressDialogStatus(true)
                }, {
                    loadClientList()
                }, {
                    view?.progressDialogStatus(false)
                    BusProvider.post(ToastMessage(it))
                }))
    }

    private fun loadClientList() {
        val query = ParseQuery.getQuery(Client::class.java)
        parse.findSomething(query, UpdateTime.AUTOLOW, ParseModel.CLIENT,
                LoadCallback<List<Client>>({

                }, {
                    loadCategoryList()
                }, {
                    view?.progressDialogStatus(false)
                    BusProvider.post(ToastMessage(it))
                }))
    }

    private fun loadCategoryList() {
        val query = ParseQuery.getQuery(Category::class.java)
        query.orderByAscending("order")
        parse.findSomething(query, UpdateTime.AUTOLOW, ParseModel.CATEGORY,
                LoadCallback<List<Category>>({

                }, {
                    loadAddressList()
                }, {
                    view?.progressDialogStatus(false)
                    BusProvider.post(ToastMessage(it))
                }))
    }

    private fun loadAddressList() {
        val query = ParseQuery.getQuery(Address::class.java)
        parse.findSomething(query, UpdateTime.AUTOLOW, ParseModel.ADDRESS,
                LoadCallback<List<Address>>({

                }, {
                    loadPromoList();
                }, {
                    view?.progressDialogStatus(false)
                    BusProvider.post(ToastMessage(it))
                }))
    }

    private fun loadPromoList() {
        val query = ParseQuery.getQuery(Product::class.java)
        parse.findSomething(query, UpdateTime.AUTOLOW, ParseModel.PRODUCT,
                LoadCallback<List<Product>>({
                    if (it == LoadStatus.LOADED) view?.progressDialogStatus(false)
                }, {
                    view?.clearAdapter()
                    view?.setItemListAsSlideshow(it.filter { it.type.equals("slideshow") })
                    view?.setItemListAsPromo(it.filter { it.type.equals("promo") })
                }, {
                    view?.progressDialogStatus(false)
                    BusProvider.post(ToastMessage(it))
                }))
    }

    override fun onStart() {

    }

    override fun onStop() {

    }

    interface View {

        fun progressDialogStatus(status: Boolean)

        fun clearAdapter();

        fun setItemListAsSlideshow(list: List<Product>);

        fun setItemListAsPromo(list: List<Product>);

    }

}
