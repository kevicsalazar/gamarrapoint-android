package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.kevicsalazar.appkit.BaseFragment
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.R
import kotlinx.android.synthetic.item_imageview_fill.*

/**
 * Created by Kevin Salazar
 */
class SlideshowFragment : BaseFragment() {

    companion object {
        @JvmStatic fun show(imageUrl: String?): SlideshowFragment {
            var bundle = Bundle()
            bundle.putString("imageUrl", imageUrl)
            var fragment = SlideshowFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(context).load(arguments.getString("imageUrl") ?: "http://gp.com/placeholder.jpg").into(ivSlideshow);
    }

    override val fragmentLayout: Int = R.layout.item_imageview_fill

    override val presenter: BasePresenter? get() = null

    override fun setUpComponent() {

    }

}