package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.os.Bundle
import android.view.MenuItem
import com.kevicsalazar.appkit.BaseActivity
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.BuildConfig
import com.kevicsalazar.gamarrapoint.R
import kotlinx.android.synthetic.activity_about.*

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class AboutActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)
        supportActionBar?.setTitle(R.string.about)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        tvAppVersion.text = getString(R.string.app_name) + " v" + BuildConfig.VERSION_NAME

    }

    override val layout: Int = R.layout.activity_about

    override val presenter: BasePresenter? get() = null

    override fun setupComponent() {

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}