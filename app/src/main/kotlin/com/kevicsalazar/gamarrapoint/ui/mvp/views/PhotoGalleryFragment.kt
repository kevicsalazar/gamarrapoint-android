package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.os.Bundle
import com.kevicsalazar.appkit.BaseFragment
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.R

/**
 * Created by Kevin Salazar
 */
class PhotoGalleryFragment : BaseFragment() {

    companion object {
        @JvmStatic fun show(imageUrl: String?): PhotoGalleryFragment {
            var bundle = Bundle()
            bundle.putString("imageUrl", imageUrl)
            var fragment = PhotoGalleryFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override val fragmentLayout: Int = R.layout.item_imageview

    override val presenter: BasePresenter? get() = null

    override fun setUpComponent() {

    }

}