package com.kevicsalazar.gamarrapoint.ui.mvp.model

import com.parse.ParseObject

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
object ParseModel {

    const val ADDRESS = "Address"
    const val PRODUCT = "Product"
    const val CLIENT = "Client"
    const val CATEGORY = "Category"
    const val PHOTO = "Photo"
    const val CATEGORY_CLIENT = "CategoryClient"

    fun register() {
        ParseObject.registerSubclass(Address::class.java);
        ParseObject.registerSubclass(Product::class.java);
        ParseObject.registerSubclass(Client::class.java);
        ParseObject.registerSubclass(Category::class.java);
        ParseObject.registerSubclass(Photo::class.java);
        ParseObject.registerSubclass(CategoryClient::class.java);
    }

}
