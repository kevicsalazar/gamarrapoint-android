package com.kevicsalazar.gamarrapoint.ui.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.View

import com.kevicsalazar.gamarrapoint.R

import kotlinx.android.synthetic.item_empty.view.*
import jp.satorufujiwara.binder.recycler.RecyclerBinder

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class EmptyRecyclerBinder(activity: Activity, val message: String) : RecyclerBinder<BinderViewType>(activity, BinderViewType.EMPTY) {

    override fun layoutResId(): Int {
        return R.layout.item_empty
    }

    override fun onCreateViewHolder(v: View): RecyclerView.ViewHolder {
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(holder as ViewHolder) {
            itemView.tvMessage.text = message
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

}
