package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.kevicsalazar.appkit.BaseActivity
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.adapters.BinderAdapter
import com.kevicsalazar.gamarrapoint.ui.adapters.BinderSection
import com.kevicsalazar.gamarrapoint.ui.adapters.ProductInfoRecyclerBinder
import com.kevicsalazar.gamarrapoint.ui.di.ActivityComponent
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Client
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Product
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.ProductDetailPresenter
import kotlinx.android.synthetic.activity_product_detail.*
import javax.inject.Inject

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class ProductDetailActivity : BaseActivity(), ProductDetailPresenter.View {

    @Inject lateinit var productDetailPresenter: ProductDetailPresenter

    var binderAdapter = BinderAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        productDetailPresenter.view = this

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.setHasFixedSize(true)
        recycler.adapter = binderAdapter

        Glide.with(this).load(intent.getStringExtra("imageUrl")).into(backdrop)
        productDetailPresenter.getClientDetail(intent.getStringExtra("clientId"))
        productDetailPresenter.getProductDetail(intent.getStringExtra("productId"))
    }

    override val layout: Int = R.layout.activity_product_detail

    override val presenter: BasePresenter? get() = productDetailPresenter

    override fun setupComponent() {
        ActivityComponent.Initializer.init(this).inject(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setClient(client: Client) {
        collapsing_toolbar.title = client.name
    }

    override fun clearAdapter() {
        binderAdapter.clear()
    }

    override fun showProductInfo(product: Product) {
        binderAdapter.add(BinderSection.PRODUCT_DETAIL, ProductInfoRecyclerBinder(this, product))
    }

}