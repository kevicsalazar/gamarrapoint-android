package com.kevicsalazar.gamarrapoint.ui.adapters

import jp.satorufujiwara.binder.ViewType

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
enum class BinderViewType : ViewType {

    PROMO,
    MAIN_PROMO,
    CLIENT,
    SHOP_DETAIL,
    SHOP_GALLERY,
    SHOP_ADDRESS,
    PRODUCT_INFO,
    EMPTY;

    override fun viewType(): Int = ordinal

}