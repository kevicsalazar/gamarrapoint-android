package com.kevicsalazar.gamarrapoint.ui.mvp.model

import com.parse.ParseClassName
import com.parse.ParseFile
import com.parse.ParseObject

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@ParseClassName(ParseModel.PHOTO)
class Photo : ParseObject() {

    val photo: ParseFile?
        get() = getParseFile("photo")

    val client: Client
        get() = getParseObject("client") as Client

}