package com.kevicsalazar.gamarrapoint.ui.mvp.model

import com.parse.ParseClassName
import com.parse.ParseFile
import com.parse.ParseObject

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@ParseClassName(ParseModel.PRODUCT)
class Product : ParseObject() {

    val title: String
        get() = getString("title") ?: ""

    val description: String
        get() = getString("description") ?: ""

    val price: Double
        get() = getNumber("price").toDouble()

    val discount: Double
        get() = getNumber("discount").toDouble()

    val type: String
        get() = getString("type") ?: "default"

    val image: ParseFile?
        get() = getParseFile("image")

    val enable: Boolean
        get() = getBoolean("enable")

    val client: Client
        get() = getParseObject("client") as Client

}
