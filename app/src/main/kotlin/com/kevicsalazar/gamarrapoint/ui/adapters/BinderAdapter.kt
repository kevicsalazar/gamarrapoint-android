package com.kevicsalazar.gamarrapoint.ui.adapters

import jp.satorufujiwara.binder.recycler.RecyclerBinderAdapter

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class BinderAdapter : RecyclerBinderAdapter<BinderSection, BinderViewType>()