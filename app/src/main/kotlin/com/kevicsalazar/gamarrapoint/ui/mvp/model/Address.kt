package com.kevicsalazar.gamarrapoint.ui.mvp.model

import com.parse.ParseClassName
import com.parse.ParseGeoPoint
import com.parse.ParseObject

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@ParseClassName(ParseModel.ADDRESS)
class Address : ParseObject() {

    val address: String
        get() = getString("address") ?: ""

    val location: ParseGeoPoint
        get() = getParseGeoPoint("location")

    val client: Client
        get() = getParseObject("client") as Client

}