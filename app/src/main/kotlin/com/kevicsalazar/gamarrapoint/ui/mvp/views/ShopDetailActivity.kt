package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.os.Bundle
import android.view.MenuItem
import com.kevicsalazar.appkit.BaseActivity
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.adapters.SimplePagerAdapter
import com.kevicsalazar.gamarrapoint.ui.di.ActivityComponent
import kotlinx.android.synthetic.activity_shop_detail.*

/**
 * Created by Kevin Salazar
 */
class ShopDetailActivity : BaseActivity() {

    var adapter: SimplePagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)

        supportActionBar?.title = intent.getStringExtra("name")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        adapter = SimplePagerAdapter(supportFragmentManager)

        adapter?.addFragment(ProductListFragment.show(intent.getStringExtra("clientId")), getString(R.string.products))
        adapter?.addFragment(ShopProfileFragment.show(intent.getStringExtra("clientId")), getString(R.string.profile))
        viewpager.adapter = adapter
        tabs.post({ tabs.setupWithViewPager(viewpager) })
    }

    override val layout: Int = R.layout.activity_shop_detail

    override val presenter: BasePresenter? get() = null

    override fun setupComponent() {
        ActivityComponent.Initializer.init(this).inject(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}