package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.os.Bundle
import android.view.View
import com.kevicsalazar.appkit.BaseFragment
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.di.ActivityComponent
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.MainPresenter
import kotlinx.android.synthetic.fragment_wishlist.*
import javax.inject.Inject

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class WishListFragment : BaseFragment() {

    @Inject lateinit var mainPresenter: MainPresenter

    companion object {
        @JvmStatic fun show(): WishListFragment {
            return WishListFragment()
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupActionBar(toolbar)
        setTitle(R.string.wish_list)
        mainPresenter.view = activity as MainActivity

        mainPresenter.setupDrawerToggle(toolbar)
    }

    override val fragmentLayout: Int = R.layout.fragment_wishlist

    override val presenter: BasePresenter? get() = null

    override fun setUpComponent() {
        ActivityComponent.Initializer.init(activity).inject(this)
    }

    override fun onStart() {
        super.onStart()
        mainPresenter.closeDrawerMenu()
    }

}