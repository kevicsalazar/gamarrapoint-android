package com.kevicsalazar.gamarrapoint.ui.mvp.model

import com.parse.ParseClassName
import com.parse.ParseObject

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@ParseClassName(ParseModel.CATEGORY)
class Category : ParseObject() {

    val name: String
        get() = getString("name") ?: ""

    val order: Int
        get() = getInt("order")

}