package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.animation.AnimationUtils
import com.kevicsalazar.appkit.BaseActivity
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.di.ActivityComponent
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.LoginPresenter
import com.parse.ParseFacebookUtils
import kotlinx.android.synthetic.activity_login.*
import org.jetbrains.anko.onClick
import javax.inject.Inject

/**
 * Created by Kevin Salazar
 */
class LoginActivity : BaseActivity(), LoginPresenter.View {

    @Inject lateinit var loginPresenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginPresenter.view = this
        btnLoginWithFacebook.supportBackgroundTintList = ContextCompat.getColorStateList(this, R.color.color_facebook)
        var animScaleDown = AnimationUtils.loadAnimation(this, R.anim.scale_down);
        var animSlideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        background.startAnimation(animScaleDown)
        btnLoginWithFacebook.startAnimation(animSlideUp)
        btnLoginWithFacebook.onClick { loginPresenter.loginWithFacebook(this) }
    }

    override val layout: Int = R.layout.activity_login

    override val presenter: BasePresenter? get() = loginPresenter

    override fun setupComponent() {
        ActivityComponent.Initializer.init(this).inject(this)
    }

    override fun goToMainActivity() {
        onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }

}