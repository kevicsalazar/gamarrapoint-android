package com.kevicsalazar.gamarrapoint.ui.di

import android.app.Activity
import com.kevicsalazar.gamarrapoint.App
import com.kevicsalazar.gamarrapoint.AppComponent
import com.kevicsalazar.appkit.scopes.PerActivity
import com.kevicsalazar.gamarrapoint.ui.mvp.views.*
import dagger.Component

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@PerActivity
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(ActivityModule::class))
public interface ActivityComponent {

    // Activities

    fun inject(activity: MainActivity)

    fun inject(activity: LoginActivity)

    fun inject(activity: ShopDetailActivity)

    fun inject(activity: ProductDetailActivity)

    // Fragment

    fun inject(fragment: HomeFragment) // MainActivity

    fun inject(fragment: ShopsFragment) // MainActivity

    fun inject(fragment: ShopListFragment) // MainActivity

    fun inject(fragment: ProductListFragment) // MainActivity

    fun inject(fragment: MapaFragment) // MainActivity

    fun inject(fragment: WishListFragment) // MainActivity

    fun inject(fragment: ShopProfileFragment) // ShopDetailActivity

    object Initializer {
        fun init(activity: Activity): ActivityComponent =
                DaggerActivityComponent.builder()
                        .appComponent((activity.application as App).appComponent)
                        .activityModule(ActivityModule()).build()
    }
}
