package com.kevicsalazar.gamarrapoint.ui.mvp.presenters

import android.app.Activity
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.appkit.DrawerPresenter
import com.kevicsalazar.gamarrapoint.storage.PreferenceProvider
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.events.ToastMessage
import com.kevicsalazar.gamarrapoint.ui.mvp.views.*
import com.kevicsalazar.gamarrapoint.utils.BusProvider
import com.kevicsalazar.gamarrapoint.utils.NetworkService
import com.parse.ParseObject
import com.parse.ParseUser

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class MainPresenter(val status: NetworkService, val pref: PreferenceProvider) : DrawerPresenter {

    var closed: Boolean = false;
    var idSelected = R.id.nav_home

    var view: View? = null

    override fun setupDrawerToggle(toolbar: Toolbar) {
        view?.syncStateToggle(toolbar)
    }

    fun checkIfUserIsLogged() {
        if (isLogged()) {
            view?.changeLogoutVisibility(true)
            view?.addHeaderToNavView(R.layout.header_nav)
        } else {
            view?.changeLogoutVisibility(false)
            view?.addHeaderToNavView(R.layout.header_nav_default)
        }
    }

    fun setupNavigationIcons() {
        view?.addIconToNavView(R.id.nav_home, R.drawable.ic_view_dashboard);
        view?.addIconToNavView(R.id.nav_shops, R.drawable.ic_store);
        view?.addIconToNavView(R.id.nav_products, R.drawable.ic_shopping);
        view?.addIconToNavView(R.id.nav_map, R.drawable.ic_map);
        view?.addIconToNavView(R.id.nav_wish_list, R.drawable.ic_marker_check);
        view?.addIconToNavView(R.id.nav_userguide, R.drawable.ic_help_circle);
        view?.addIconToNavView(R.id.nav_about, R.drawable.ic_information);
        view?.addIconToNavView(R.id.nav_logout, R.drawable.ic_logout);
    }

    override fun openDrawerItemView(menuItem: MenuItem): Boolean {
        if (idSelected != menuItem.itemId) {

            when (menuItem.itemId) {
                R.id.nav_home -> view?.showFragmentView(HomeFragment.show())
                R.id.nav_shops -> view?.showFragmentView(ShopsFragment.show())
                R.id.nav_products -> view?.showFragmentView(ProductListFragment.show())
                R.id.nav_map -> view?.showFragmentView(MapaFragment.show())
                R.id.nav_wish_list -> if (isLogged()) view?.showFragmentView(WishListFragment.show())
                R.id.nav_userguide -> view?.showActivity(UserGuideActivity())
                R.id.nav_about -> view?.showActivity(AboutActivity())
                R.id.nav_logout -> {
                    if (status.isOnline) {
                        ParseUser.logOutInBackground({ e ->
                            if (e == null) {
                                pref.clear()
                                ParseObject.unpinAllInBackground();
                                view?.changeLogoutVisibility(false)
                                view?.closeDrawer(GravityCompat.START)
                                view?.checkedNavViewItem(R.id.nav_home)
                                view?.showFragmentView(HomeFragment.show())
                                view?.addHeaderToNavView(R.layout.header_nav_default)
                            }
                        });
                    } else {
                        BusProvider.post(ToastMessage(R.string.error_network))
                    }
                }
            }

            when (menuItem.itemId) {
                R.id.nav_wish_list -> {
                    if (isLogged()) {
                        idSelected = menuItem.itemId
                        return true
                    } else {
                        view?.showActivity(LoginActivity())
                        return false
                    }
                }
                else -> {
                    if (menuItem.groupId != R.id.nav_options) {
                        idSelected = menuItem.itemId
                    }
                    return true
                }
            }

        } else {
            view?.closeDrawer(GravityCompat.START)
            return false
        }
    }

    override fun closeDrawerMenu() {
        if (!closed) {
            closed = true
            view?.closeDrawer(GravityCompat.START)
        }
    }

    override fun onStart() {

    }

    override fun onStop() {

    }

    interface View {

        fun syncStateToggle(toolbar: Toolbar)

        fun addHeaderToNavView(headerId: Int)

        fun changeLogoutVisibility(visible: Boolean)

        fun checkedNavViewItem(itemId: Int)

        fun addIconToNavView(resId: Int, iconId: Int)

        fun showFragmentView(fragment: Fragment)

        fun showActivity(activity: Activity)

        fun closeDrawer(gravity: Int)

    }

    private fun isLogged(): Boolean = ParseUser.getCurrentUser() != null

}