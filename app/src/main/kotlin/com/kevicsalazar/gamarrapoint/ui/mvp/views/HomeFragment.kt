package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import cn.pedant.SweetAlert.SweetAlertDialog
import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.appkit.BaseFragment
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.ui.adapters.BinderAdapter
import com.kevicsalazar.gamarrapoint.ui.adapters.BinderSection
import com.kevicsalazar.gamarrapoint.ui.adapters.ProductRecyclerBinder
import com.kevicsalazar.gamarrapoint.ui.adapters.SlideshowRecyclerBinder
import com.kevicsalazar.gamarrapoint.ui.di.ActivityComponent
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Product
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.HomePresenter
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.MainPresenter
import com.kevicsalazar.gamarrapoint.utils.ext.getSweetProgress
import kotlinx.android.synthetic.fragment_home.*
import javax.inject.Inject

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class HomeFragment : BaseFragment(), HomePresenter.View {

    @Inject lateinit var mainPresenter: MainPresenter
    @Inject lateinit var homePresenter: HomePresenter

    var binderAdapter = BinderAdapter()

    var pDialog: SweetAlertDialog? = null

    companion object {
        @JvmStatic fun show(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupActionBar(toolbar)
        setTitle(R.string.home)

        mainPresenter.view = activity as MainActivity
        homePresenter.view = this

        pDialog = context.getSweetProgress(getString(R.string.update_data))

        mainPresenter.setupDrawerToggle(toolbar)

        var layoutManager = GridLayoutManager(context, 2)
        layoutManager.spanSizeLookup = SpanSizeLookup()
        recycler.layoutManager = layoutManager
        recycler.setHasFixedSize(true)

        recycler.adapter = binderAdapter
        homePresenter.loadData()
    }

    override val fragmentLayout: Int = R.layout.fragment_home

    override val presenter: BasePresenter? get() = homePresenter

    override fun setUpComponent() {
        ActivityComponent.Initializer.init(activity).inject(this)
    }

    override fun onStart() {
        super.onStart()
        mainPresenter.closeDrawerMenu()
    }

    override fun progressDialogStatus(status: Boolean) {
        if (status) {
            pDialog?.show()
        } else {
            if (pDialog?.isShowing as Boolean) {
                pDialog?.dismissWithAnimation()
            }
        }
    }

    override fun clearAdapter() {
        binderAdapter.clear();
    }

    override fun setItemListAsSlideshow(list: List<Product>) {
        binderAdapter.add(BinderSection.PROMO, SlideshowRecyclerBinder(activity, list))
    }

    override fun setItemListAsPromo(list: List<Product>) {
        list.forEach { binderAdapter.add(BinderSection.PROMO, ProductRecyclerBinder(activity, it)) }
    }

    private class SpanSizeLookup : GridLayoutManager.SpanSizeLookup() {

        override fun getSpanSize(position: Int): Int {
            when (position) {
                0 -> return 2
                else -> return 1
            }
        }
    }

}