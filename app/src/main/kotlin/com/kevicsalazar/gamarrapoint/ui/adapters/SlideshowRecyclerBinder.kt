package com.kevicsalazar.gamarrapoint.ui.adapters

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import com.kevicsalazar.appkit.views.PageIndicator

import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Product
import com.kevicsalazar.gamarrapoint.ui.mvp.views.SlideshowFragment

import kotlinx.android.synthetic.item_slideshow.view.*
import jp.satorufujiwara.binder.recycler.RecyclerBinder
import org.jetbrains.anko.onUiThread
import java.util.*
import kotlin.concurrent.timer

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class SlideshowRecyclerBinder(val act: Activity, val list: List<Product>) : RecyclerBinder<BinderViewType>(act, BinderViewType.MAIN_PROMO) {

    override fun layoutResId(): Int {
        return R.layout.item_slideshow
    }

    override fun onCreateViewHolder(v: View): RecyclerView.ViewHolder {
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as ViewHolder
        holder.bindSlideshow(list, act as AppCompatActivity)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var t: Timer? = null

        fun bindSlideshow(list: List<Product>, act: AppCompatActivity) {
            var adapter = SimplePagerAdapter(act.supportFragmentManager)
            list.forEach { adapter.addFragment(SlideshowFragment.show(it.image?.url)) }
            itemView.viewpager.adapter = adapter

            itemView.indicator.setIndicatorType(PageIndicator.IndicatorType.CIRCLE);
            itemView.indicator.setViewPager(itemView.viewpager)

            if (t == null) {
                t = timer(initialDelay = 4000, period = 4000, action = {
                    act.onUiThread {
                        var currentPage = itemView.viewpager.currentItem
                        if (currentPage == list.size - 1) {
                            currentPage = 0;
                        } else {
                            currentPage += 1
                        }
                        itemView.viewpager.setCurrentItem(currentPage, true)
                    }
                })
            }

        }

    }

}
