package com.kevicsalazar.gamarrapoint.ui.mvp.presenters

import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Client
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Product
import com.parse.ParseObject
import com.parse.ParseQuery

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class ProductListPresenter : BasePresenter {

    var view: View? = null

    fun getProductList(clientId: String) {
        val query = ParseQuery.getQuery(Product::class.java)
        if (clientId.isNotEmpty()) {
            query.whereEqualTo("client", ParseObject.createWithoutData(Client::class.java, clientId))
        }
        query.fromLocalDatastore()
        query.findInBackground { list, e ->
            if (e == null) {
                view?.clearAdapter()
                view?.addItemListAsProduct(list.filter { it.type.equals("promo") || it.type.equals("default") })
            }
        }
    }

    override fun onStart() {

    }

    override fun onStop() {

    }

    interface View {

        fun clearAdapter()

        fun addItemListAsProduct(list: List<Product>)

    }
}