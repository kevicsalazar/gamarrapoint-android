package com.kevicsalazar.gamarrapoint.ui.adapters

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View

import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Photo
import com.kevicsalazar.gamarrapoint.ui.mvp.views.PhotoGalleryFragment

import kotlinx.android.synthetic.item_shop_gallery.view.*
import jp.satorufujiwara.binder.recycler.RecyclerBinder

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class PhotoRecyclerBinder(val act: Activity, val list: List<Photo>) : RecyclerBinder<BinderViewType>(act, BinderViewType.SHOP_GALLERY) {

    override fun layoutResId(): Int {
        return R.layout.item_shop_gallery
    }

    override fun onCreateViewHolder(v: View): RecyclerView.ViewHolder {
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as ViewHolder
        holder.bindSlideshow(list, act as AppCompatActivity)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindSlideshow(list: List<Photo>, act: AppCompatActivity) {
            var adapter = SimplePagerAdapter(act.supportFragmentManager)
            list.forEach {
                adapter.addFragment(PhotoGalleryFragment.show(it.photo?.url))
            }
            itemView.vpGallery.adapter = adapter
        }
    }

}
