package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.kevicsalazar.appkit.BaseFragment
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.adapters.*
import com.kevicsalazar.gamarrapoint.ui.di.ActivityComponent
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Address
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Client
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Photo
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.ShopProfilePresenter
import kotlinx.android.synthetic.fragment_shop_profile.*
import javax.inject.Inject

/**
 * Created by Kevin Salazar
 */
class ShopProfileFragment : BaseFragment(), ShopProfilePresenter.View {

    @Inject lateinit var shopProfilePresenter: ShopProfilePresenter
    var binderAdapter = BinderAdapter()

    companion object {
        @JvmStatic fun show(clientId: String?): ShopProfileFragment {
            var bundle = Bundle()
            bundle.putString("clientId", clientId)
            var fragment = ShopProfileFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shopProfilePresenter.view = this
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.adapter = binderAdapter
        shopProfilePresenter.loadShopProfile(arguments.getString("clientId"))
    }

    override val fragmentLayout: Int = R.layout.fragment_shop_profile

    override val presenter: BasePresenter? get() = shopProfilePresenter

    override fun setUpComponent() {
        ActivityComponent.Initializer.init(activity).inject(this)
    }

    override fun showShopDetail(client: Client) {
        binderAdapter.add(BinderSection.SHOP_PROFILE, ShopDetailRecyclerBinder(activity, client))
    }

    override fun showShopAddress(list: List<Address>) {
        binderAdapter.add(BinderSection.SHOP_PROFILE, AddressRecyclerBinder(activity, list))
    }

    override fun showShopGallery(list: List<Photo>) {
        binderAdapter.add(BinderSection.SHOP_PROFILE, PhotoRecyclerBinder(activity, list))
    }

}