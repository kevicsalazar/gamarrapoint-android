package com.kevicsalazar.gamarrapoint.ui.mvp.presenters

import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.appkit.callbacks.LoadCallback
import com.kevicsalazar.gamarrapoint.cloud.ParseLoadAction
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Category
import com.kevicsalazar.gamarrapoint.ui.mvp.model.CategoryClient
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.events.ToastMessage
import com.kevicsalazar.gamarrapoint.utils.BusProvider
import com.parse.ParseObject
import com.parse.ParseQuery

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class ClientListPresenter(val parse: ParseLoadAction) : BasePresenter {

    var view: View? = null

    fun loadClientList(categoryId: String) {
        var category = ParseObject.createWithoutData(Category::class.java, categoryId)
        var query = ParseQuery.getQuery(CategoryClient::class.java)
        query.whereEqualTo("category", category)
        query.fromLocalDatastore()
        parse.findSomething(query,
                LoadCallback<List<CategoryClient>>({

                }, {
                    view?.clearAdapter()
                    view?.addItemListAsClient(it)
                }, {
                    BusProvider.post(ToastMessage(it))
                }));
    }

    override fun onStart() {

    }

    override fun onStop() {

    }

    interface View {

        fun clearAdapter()

        fun addItemListAsClient(list: List<CategoryClient>)

    }
}