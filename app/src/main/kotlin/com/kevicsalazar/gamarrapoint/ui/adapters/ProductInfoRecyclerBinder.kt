package com.kevicsalazar.gamarrapoint.ui.adapters

import android.app.Activity
import android.content.Context
import android.graphics.Paint
import android.support.v7.widget.RecyclerView
import android.view.View
import com.kevicsalazar.appkit.utils.ext.format

import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Product

import kotlinx.android.synthetic.item_product_info.view.*
import jp.satorufujiwara.binder.recycler.RecyclerBinder

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class ProductInfoRecyclerBinder(activity: Activity, val product: Product) : RecyclerBinder<BinderViewType>(activity, BinderViewType.PRODUCT_INFO) {

    override fun layoutResId(): Int {
        return R.layout.item_product_info
    }

    override fun onCreateViewHolder(v: View): RecyclerView.ViewHolder {
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as ViewHolder
        holder.bindProductInfo(product, activity)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindProductInfo(product: Product, ctx: Context) {
            with(product) {
                itemView.tvTitle.text = title
                itemView.tvPriceWithDiscount.text = "S/." + (price - (price * discount)).format(2)
                itemView.tvPrice.text = "S/." + price.format(2)
                itemView.tvPrice.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                itemView.tvDiscount.text = "-" + (discount * 100).toInt() + " %"
            }
        }

    }

}
