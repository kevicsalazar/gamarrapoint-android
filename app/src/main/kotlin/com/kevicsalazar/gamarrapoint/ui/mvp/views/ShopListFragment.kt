package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.appkit.BaseFragment
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.ui.adapters.*
import com.kevicsalazar.gamarrapoint.ui.di.ActivityComponent
import com.kevicsalazar.gamarrapoint.ui.mvp.model.CategoryClient
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.ClientListPresenter
import kotlinx.android.synthetic.fragment_shop_list.*
import javax.inject.Inject

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class ShopListFragment : BaseFragment(), ClientListPresenter.View {

    @Inject lateinit var clientListPresenter: ClientListPresenter

    var binderAdapter = BinderAdapter()

    companion object {
        @JvmStatic fun show(categoryId: String): ShopListFragment {
            var bundle = Bundle()
            bundle.putString("categoryId", categoryId)
            var fragment = ShopListFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clientListPresenter.view = this
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.adapter = binderAdapter
        clientListPresenter.loadClientList(arguments.getString("categoryId"))
    }

    override val fragmentLayout: Int = R.layout.fragment_shop_list

    override val presenter: BasePresenter? get() = clientListPresenter

    override fun setUpComponent() {
        ActivityComponent.Initializer.init(activity).inject(this)
    }

    override fun clearAdapter() {
        binderAdapter.clear();
    }

    override fun addItemListAsClient(list: List<CategoryClient>) {
        list.forEach { binderAdapter.add(BinderSection.CLIENT, ShopRecyclerBinder(activity, it.client)) }
        binderAdapter.addIfEmpty(BinderSection.CLIENT, EmptyRecyclerBinder(activity, getString(R.string.empty_message)))
    }

}