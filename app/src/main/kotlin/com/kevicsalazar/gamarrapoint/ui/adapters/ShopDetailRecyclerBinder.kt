package com.kevicsalazar.gamarrapoint.ui.adapters

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ArrayAdapter
import com.bumptech.glide.Glide

import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Client

import kotlinx.android.synthetic.item_shop_detail.view.*
import jp.satorufujiwara.binder.recycler.RecyclerBinder
import org.jetbrains.anko.onClick

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class ShopDetailRecyclerBinder(val act: Activity, val client: Client) : RecyclerBinder<BinderViewType>(act, BinderViewType.SHOP_DETAIL) {

    override fun layoutResId(): Int {
        return R.layout.item_shop_detail
    }

    override fun onCreateViewHolder(v: View): RecyclerView.ViewHolder {
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(holder as ViewHolder) {
            itemView.tvDetail.text = client.description
            Glide.with(activity).load(client.logo?.url ?: "http://gp.com/placeholder.jpg").into(itemView.ivShopIcon)
            if (client.phoneList.size != 0) {
                val adapter = ArrayAdapter(activity, R.layout.item_textview, client.phoneList)
                itemView.spPhoneList.adapter = adapter
                itemView.btnCall.onClick {
                    val intent = Intent(Intent.ACTION_DIAL)
                    intent.setData(Uri.parse("tel:" + itemView.spPhoneList.selectedItem))
                    act.startActivity(intent)
                }
            } else {
                itemView.spPhoneList.visibility = View.GONE
                itemView.btnCall.visibility = View.GONE
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

}
