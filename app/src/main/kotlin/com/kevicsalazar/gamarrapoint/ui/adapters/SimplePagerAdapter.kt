package com.kevicsalazar.gamarrapoint.ui.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import java.util.*

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class SimplePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter (fm) {

    private val mFragments: MutableList<Fragment>
    private val mFragmentTitles: MutableList<String>

    init {
        mFragments = ArrayList<Fragment>()
        mFragmentTitles = ArrayList<String>()
    }

    fun addFragment(fragment: Fragment, title: String = "Default") {
        mFragments.add(fragment)
        mFragmentTitles.add(title)
    }

    override fun getItem(position: Int): Fragment {
        return mFragments[position]
    }

    override fun getCount(): Int {
        return mFragments.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return mFragmentTitles[position]
    }
}