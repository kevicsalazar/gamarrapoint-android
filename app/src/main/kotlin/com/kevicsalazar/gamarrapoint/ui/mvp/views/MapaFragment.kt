package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.os.Bundle
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.kevicsalazar.appkit.BaseFragment
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.di.ActivityComponent
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.MainPresenter
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Address
import com.parse.ParseQuery
import kotlinx.android.synthetic.fragment_mapa.*
import javax.inject.Inject

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class MapaFragment : BaseFragment(), OnMapReadyCallback {

    @Inject lateinit var mainPresenter: MainPresenter

    var map: GoogleMap? = null
    var mapFragment: MapFragment? = null

    companion object {
        @JvmStatic fun show(): MapaFragment {
            return MapaFragment()
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupActionBar(toolbar)
        setTitle(R.string.map)
        mainPresenter.view = activity as MainActivity

        mainPresenter.setupDrawerToggle(toolbar)

        mapFragment = activity.fragmentManager.findFragmentById(R.id.map) as MapFragment
        mapFragment?.getMapAsync(this)
    }

    override val fragmentLayout: Int = R.layout.fragment_mapa

    override val presenter: BasePresenter? get() = null

    override fun setUpComponent() {
        ActivityComponent.Initializer.init(activity).inject(this)
    }

    override fun onStart() {
        super.onStart()
        mainPresenter.closeDrawerMenu()
    }

    override fun onDetach() {
        super.onDetach()
        activity.fragmentManager.beginTransaction().remove(mapFragment).commit()
    }

    override fun onMapReady(map: GoogleMap?) {
        this.map = map
        map?.isMyLocationEnabled = true;
        map?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(-12.064442, -77.013456), 16.toFloat()));
        val query = ParseQuery.getQuery(Address::class.java)
        query.fromLocalDatastore()
        query.findInBackground { list, e ->
            if (e == null) {
                list.forEach {
                    var latLng = LatLng(it.location.latitude, it.location.longitude)
                    map?.addMarker(MarkerOptions()
                            .title(it.client.name)
                            .snippet(it.address)
                            .position(latLng))
                }
            }
        }
    }
}