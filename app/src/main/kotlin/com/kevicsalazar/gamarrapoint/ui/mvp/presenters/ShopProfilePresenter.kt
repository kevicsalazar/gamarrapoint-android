package com.kevicsalazar.gamarrapoint.ui.mvp.presenters

import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Address
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Client
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Photo
import com.parse.GetCallback
import com.parse.ParseException
import com.parse.ParseQuery

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class ShopProfilePresenter : BasePresenter {

    var view: View? = null

    fun loadShopProfile(clientId: String) {
        getDetail(clientId)
    }

    private fun getDetail(clientId: String) {
        val query = ParseQuery.getQuery(Client::class.java)
        query.fromLocalDatastore()
        query.getInBackground(clientId, object : GetCallback<Client> {
            override fun done(client: Client, e: ParseException?) {
                if (e == null) {
                    view?.showShopDetail(client)
                    getAddress(client)
                }
            }
        })
    }

    private fun getAddress(client: Client) {
        val query = ParseQuery.getQuery(Address::class.java)
        query.whereEqualTo("client", client)
        query.fromLocalDatastore()
        query.findInBackground({ list, e ->
            if (e == null) {
                view?.showShopAddress(list)
                getGallery(client)
            }
        })
    }

    private fun getGallery(client: Client) {
        /*val query = ParseQuery.getQuery(PhotoGallery::class.java)
        query.whereEqualTo("client", client)
        query.fromLocalDatastore()
        query.findInBackground({ list, e ->
            if (e == null) {
                view?.showShopGallery(list)
            }
        })*/
    }

    override fun onStart() {

    }

    override fun onStop() {

    }

    interface View {

        fun showShopDetail(client: Client)

        fun showShopAddress(list: List<Address>)

        fun showShopGallery(list: List<Photo>)

    }

}