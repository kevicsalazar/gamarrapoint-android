package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.appkit.BaseActivity
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.appkit.utils.ext.replaceContentFragment
import com.kevicsalazar.gamarrapoint.storage.PreferenceProvider
import com.kevicsalazar.gamarrapoint.ui.di.ActivityComponent
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.MainPresenter
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.events.*
import com.kevicsalazar.gamarrapoint.utils.AnalyticsProvider
import com.kevicsalazar.gamarrapoint.utils.BusProvider
import com.squareup.otto.Subscribe
import com.wnafee.vector.compat.ResourcesCompat
import jp.wasabeef.glide.transformations.CropCircleTransformation
import kotlinx.android.synthetic.activity_main.*
import kotlinx.android.synthetic.header_nav.view.*
import kotlinx.android.synthetic.header_nav_default.view.*
import org.jetbrains.anko.toast
import org.jetbrains.anko.startActivity
import javax.inject.Inject

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class MainActivity : BaseActivity(), MainPresenter.View {

    @Inject lateinit var mainPresenter: MainPresenter
    @Inject lateinit var analitycs: AnalyticsProvider
    @Inject lateinit var pref: PreferenceProvider

    var nav_header: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.MainTheme);
        super.onCreate(savedInstanceState)
        BusProvider.register(this)
        mainPresenter.view = this
        mainPresenter.setupNavigationIcons()
        nav_view.setNavigationItemSelectedListener { menuItem ->
            mainPresenter.openDrawerItemView(menuItem)
        }
        showFragmentView(HomeFragment.show())
    }

    override val layout: Int = R.layout.activity_main

    override val presenter: BasePresenter? get() = mainPresenter

    override fun setupComponent() {
        ActivityComponent.Initializer.init(this).inject(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> drawer_layout.openDrawer(GravityCompat.START)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        analitycs.trackScreenName("Abriendo Aplicación")
    }

    override fun onResume() {
        super.onResume()
        mainPresenter.checkIfUserIsLogged()
    }

    override fun syncStateToggle(toolbar: Toolbar) {
        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.open, R.string.close);
        drawer_layout.setDrawerListener(toggle);
        toggle.syncState();
    }

    override fun addHeaderToNavView(headerId: Int) {
        if (nav_header != null) nav_view.removeHeaderView(nav_header)
        nav_header = nav_view.inflateHeaderView(headerId);
        when (headerId) {
            R.layout.header_nav -> {
                nav_header?.tvTitle?.text = pref.getString("facebookUserName")
                nav_header?.tvDescription?.text = pref.getString("facebookUserEmail")
                Glide.with(this)
                        .load("https://graph.facebook.com/" + pref.getString("facebookUserId") + "/picture?width=200&height=200")
                        .bitmapTransform(CropCircleTransformation(this))
                        .into(nav_header?.ivAvatar)
            }
        }
    }

    override fun changeLogoutVisibility(visible: Boolean) {
        nav_view.menu.findItem(R.id.nav_logout).setVisible(visible)
    }

    override fun checkedNavViewItem(itemId: Int) {
        nav_view.menu.findItem(itemId).setChecked(true)
    }

    override fun addIconToNavView(resId: Int, iconId: Int) {
        nav_view.menu.findItem(resId).setIcon(ResourcesCompat.getDrawable(this, iconId));
    }

    override fun showFragmentView(fragment: Fragment) {
        replaceContentFragment(R.id.main_content, fragment)
    }

    override fun showActivity(activity: Activity) {
        when (activity) {
            is LoginActivity     -> startActivity<LoginActivity>()
            is UserGuideActivity -> startActivity<UserGuideActivity>()
            is AboutActivity     -> startActivity<AboutActivity>()
        }
        closeDrawer(GravityCompat.START)
    }

    override fun closeDrawer(gravity: Int) {
        drawer_layout.closeDrawer(gravity)
    }

    @Subscribe fun toastMessage(message: ToastMessage) {
        toast(message.id)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.unregister(this)
    }

}
