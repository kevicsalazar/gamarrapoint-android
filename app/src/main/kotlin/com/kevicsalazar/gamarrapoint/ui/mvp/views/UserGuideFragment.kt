package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import com.kevicsalazar.appkit.BaseFragment
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.appkit.model.GuideModel
import com.kevicsalazar.gamarrapoint.R
import kotlinx.android.synthetic.fragment_userguide_page.*

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class UserGuideFragment : BaseFragment() {

    companion object {
        @JvmStatic fun show(guideModel: GuideModel): UserGuideFragment {
            var bundle = Bundle()
            bundle.putInt("titleId", guideModel.titleId)
            bundle.putInt("descriptionId", guideModel.descriptionId)
            bundle.putInt("colorId", guideModel.colorId)
            bundle.putInt("screenshotId", guideModel.screenshotId)
            var fragment = UserGuideFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        wrapper.setBackgroundColor(ContextCompat.getColor(context, arguments.getInt("colorId")))
        tvTitle.text = getString(arguments.getInt("titleId"))
        tvDescription.text = getString(arguments.getInt("descriptionId"))
        mockup.setScreenshot(arguments.getInt("screenshotId"))
    }

    override val fragmentLayout: Int = R.layout.fragment_userguide_page

    override val presenter: BasePresenter? get() = null

    override fun setUpComponent() {

    }

}