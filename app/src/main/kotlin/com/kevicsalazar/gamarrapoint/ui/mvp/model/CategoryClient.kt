package com.kevicsalazar.gamarrapoint.ui.mvp.model

import com.parse.ParseClassName
import com.parse.ParseObject

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@ParseClassName(ParseModel.CATEGORY_CLIENT)
class CategoryClient : ParseObject() {

    val category: Category
        get() = getParseObject("category") as Category

    val client: Client
        get() = getParseObject("client") as Client

}