package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.SearchView
import android.text.InputType
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import com.kevicsalazar.appkit.BaseFragment
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.adapters.*
import com.kevicsalazar.gamarrapoint.ui.di.ActivityComponent
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Product
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.MainPresenter
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.ProductListPresenter
import kotlinx.android.synthetic.fragment_product_list.*
import javax.inject.Inject

/**
 * Created by Kevin Salazar
 */
class ProductListFragment : BaseFragment(), ProductListPresenter.View, SearchView.OnQueryTextListener {

    @Inject lateinit var mainPresenter: MainPresenter
    @Inject lateinit var productListPresenter: ProductListPresenter

    var binderAdapter = BinderAdapter()
    var list: List<Product> = arrayListOf()

    companion object {
        @JvmStatic fun show(clientId: String = ""): ProductListFragment {
            var bundle = Bundle()
            bundle.putString("clientId", clientId)
            var fragment = ProductListFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (activity is MainActivity) {
            setupActionBar(toolbar)
            setTitle(R.string.products)
            mainPresenter.view = activity as MainActivity
            mainPresenter.setupDrawerToggle(toolbar)
            appbar.visibility = View.VISIBLE
        }
        productListPresenter.view = this
        var layoutManager = GridLayoutManager(context, 2)
        layoutManager.spanSizeLookup = spanSizeLookup
        recycler.layoutManager = layoutManager
        recycler.setHasFixedSize(true)
        recycler.adapter = binderAdapter
        productListPresenter.getProductList(arguments.getString("clientId"))

    }

    override val fragmentLayout: Int = R.layout.fragment_product_list

    override val presenter: BasePresenter? get() = productListPresenter

    override fun setUpComponent() {
        ActivityComponent.Initializer.init(activity).inject(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.menu_search, menu)
        var searchItem = menu?.findItem(R.id.action_search)
        var mSearchView = MenuItemCompat.getActionView(searchItem) as SearchView
        mSearchView.setOnQueryTextListener(this)
        mSearchView.queryHint = getString(R.string.search)
        mSearchView.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
    }

    override fun onStart() {
        super.onStart()
        mainPresenter.closeDrawerMenu()
    }

    override fun clearAdapter() {
        binderAdapter.clear()
    }

    override fun addItemListAsProduct(list: List<Product>) {
        if (this.list.isEmpty()) this.list = list;
        list.forEach { binderAdapter.add(BinderSection.PRODUCT_LIST, ProductRecyclerBinder(activity, it)) }
        binderAdapter.addIfEmpty(BinderSection.PRODUCT_LIST, EmptyRecyclerBinder(activity, getString(R.string.empty_message)))
    }

    override fun onQueryTextSubmit(s: String?): Boolean {
        if (s != null) {
            binderAdapter.clear()
            addItemListAsProduct(list.filter { it.title.contains(s) })
        }
        return false
    }

    override fun onQueryTextChange(s: String?): Boolean {
        if (s != null) {
            binderAdapter.clear()
            addItemListAsProduct(list.filter { it.title.contains(s) })
        }
        return false
    }

    var spanSizeLookup: GridLayoutManager.SpanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
        override fun getSpanSize(position: Int): Int {
            if (binderAdapter.getItem(position).viewType == BinderViewType.EMPTY) {
                return 2;
            } else {
                return 1;
            }
        }
    }

}