package com.kevicsalazar.gamarrapoint.ui.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide

import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Client
import com.kevicsalazar.gamarrapoint.ui.mvp.views.ShopDetailActivity

import kotlinx.android.synthetic.item_client.view.*
import jp.satorufujiwara.binder.recycler.RecyclerBinder
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class ShopRecyclerBinder(val act: Activity, val client: Client) : RecyclerBinder<BinderViewType>(act, BinderViewType.CLIENT) {

    override fun layoutResId(): Int {
        return R.layout.item_client
    }

    override fun onCreateViewHolder(v: View): RecyclerView.ViewHolder {
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(holder as ViewHolder) {
            itemView.tag = client
            itemView.tvName.text = client.name
            itemView.tvDescription.text = client.description
            Glide.with(activity).load(client.logo?.url ?: "http://gp.com/placeholder.jpg").into(itemView.ivLogo)
            itemView.onClick { act.startActivity<ShopDetailActivity>("clientId" to client.objectId, "name" to client.name) }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

}
