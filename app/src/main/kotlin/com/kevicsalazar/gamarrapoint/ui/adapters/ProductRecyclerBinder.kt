package com.kevicsalazar.gamarrapoint.ui.adapters

import android.app.Activity
import android.graphics.Paint
import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.kevicsalazar.appkit.utils.ext.format

import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Product
import com.kevicsalazar.gamarrapoint.ui.mvp.views.ProductDetailActivity

import kotlinx.android.synthetic.item_promo.view.*
import jp.satorufujiwara.binder.recycler.RecyclerBinder
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class ProductRecyclerBinder(val act: Activity, val product: Product) : RecyclerBinder<BinderViewType>(act, BinderViewType.PROMO) {

    override fun layoutResId(): Int {
        return R.layout.item_promo
    }

    override fun onCreateViewHolder(v: View): RecyclerView.ViewHolder {
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as ViewHolder
        holder.bindPromo(product, act)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindPromo(product: Product, act: Activity) {
            with(product) {
                itemView.tvTitle.text = title
                itemView.tvPriceWithDiscount.text = "S/." + (price - (price * discount)).format(2) + " "
                itemView.tvPrice.text = " S/." + price.format(2)
                itemView.tvPrice.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                itemView.tvDiscount.text = "-" + (discount * 100).toInt() + " %"
                var imageUrl = image?.url ?: "http://gp.com/placeholder.jpg"
                Glide.with(act).load(imageUrl).into(itemView.ivImage)
                itemView.onClick { act.startActivity<ProductDetailActivity>("imageUrl" to imageUrl, "clientId" to client.objectId, "productId" to objectId) }
            }
        }

    }

}
