package com.kevicsalazar.gamarrapoint.ui.mvp.views

import android.os.Bundle
import android.view.View
import com.kevicsalazar.appkit.BaseFragment
import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.adapters.SimplePagerAdapter
import com.kevicsalazar.gamarrapoint.ui.di.ActivityComponent
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Category
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.ShopsPresenter
import com.kevicsalazar.gamarrapoint.ui.mvp.presenters.MainPresenter
import com.kevicsalazar.gamarrapoint.utils.ResourcesProvider
import kotlinx.android.synthetic.fragment_shops.*
import javax.inject.Inject

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class ShopsFragment : BaseFragment(), ShopsPresenter.View {

    @Inject lateinit var res: ResourcesProvider
    @Inject lateinit var mainPresenter: MainPresenter
    @Inject lateinit var shopsPresenter: ShopsPresenter

    var adapter: SimplePagerAdapter? = null

    companion object {
        @JvmStatic fun show(): ShopsFragment {
            return ShopsFragment()
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupActionBar(toolbar)
        setTitle(R.string.shops)

        mainPresenter.view = activity as MainActivity
        shopsPresenter.view = this

        mainPresenter.setupDrawerToggle(toolbar)
        adapter = SimplePagerAdapter(fragmentManager)
        shopsPresenter.loadCategoryList()
    }

    override val fragmentLayout: Int = R.layout.fragment_shops

    override val presenter: BasePresenter? get() = shopsPresenter

    override fun setUpComponent() {
        ActivityComponent.Initializer.init(activity).inject(this)
    }

    override fun onStart() {
        super.onStart()
        mainPresenter.closeDrawerMenu()
    }

    override fun addItemsLikeTabs(list: List<Category>) {
        list.forEach { adapter?.addFragment(ShopListFragment.show(it.objectId), it.name) }
        viewpager.adapter = adapter
        tabs.post({ tabs.setupWithViewPager(viewpager) })
    }

}