package com.kevicsalazar.gamarrapoint.ui.adapters

import jp.satorufujiwara.binder.Section

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
enum class BinderSection : Section {

    PROMO,
    SHOP_PROFILE,
    PRODUCT_LIST,
    PRODUCT_DETAIL,
    CLIENT;

    override fun position(): Int = ordinal
}