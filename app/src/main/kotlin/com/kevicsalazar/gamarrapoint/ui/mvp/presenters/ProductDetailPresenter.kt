package com.kevicsalazar.gamarrapoint.ui.mvp.presenters

import com.kevicsalazar.appkit.BasePresenter
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Client
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Product
import com.parse.GetCallback
import com.parse.ParseException
import com.parse.ParseQuery

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class ProductDetailPresenter : BasePresenter {

    var view: View? = null

    fun getClientDetail(clientId: String) {
        val query = ParseQuery.getQuery(Client::class.java)
        query.fromLocalDatastore()
        query.getInBackground(clientId, object : GetCallback<Client> {
            override fun done(client: Client, e: ParseException?) {
                if (e == null) {
                    view?.setClient(client)
                }
            }
        })
    }

    fun getProductDetail(productId: String) {
        val query = ParseQuery.getQuery(Product::class.java)
        query.fromLocalDatastore()
        query.getInBackground(productId, object : GetCallback<Product> {
            override fun done(product: Product, e: ParseException?) {
                if (e == null) {
                    view?.clearAdapter()
                    view?.showProductInfo(product)
                }
            }
        })
    }

    override fun onStart() {

    }

    override fun onStop() {

    }

    interface View {

        fun setClient(client: Client);

        fun clearAdapter()

        fun showProductInfo(product: Product)

    }

}