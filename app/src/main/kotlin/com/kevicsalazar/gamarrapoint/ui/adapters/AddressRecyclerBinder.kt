package com.kevicsalazar.gamarrapoint.ui.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

import com.kevicsalazar.gamarrapoint.R
import com.kevicsalazar.gamarrapoint.ui.mvp.model.Address

import kotlinx.android.synthetic.item_shop_location.view.*
import jp.satorufujiwara.binder.recycler.RecyclerBinder

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class AddressRecyclerBinder(activity: Activity, val list: List<Address>) : RecyclerBinder<BinderViewType>(activity, BinderViewType.SHOP_ADDRESS) {

    override fun layoutResId(): Int {
        return R.layout.item_shop_location
    }

    override fun onCreateViewHolder(v: View): RecyclerView.ViewHolder {
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(holder as ViewHolder) {
            val mapFragment = activity.fragmentManager.findFragmentById(R.id.map) as MapFragment;
            mapFragment.getMapAsync { googleMap ->
                if (list.size > 0) {
                    with(list.first()) {
                        itemView.tvAddress.text = address
                        kotlin.with(list.first().location) {
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude, longitude), 18.toFloat()))
                        }
                    }
                    list.forEach {
                        with(it.location) {
                            googleMap.addMarker(MarkerOptions().position(LatLng(latitude, longitude)));
                        }
                    }
                }
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

}
