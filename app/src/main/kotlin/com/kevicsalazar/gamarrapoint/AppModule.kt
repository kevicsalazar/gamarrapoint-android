package com.kevicsalazar.gamarrapoint

import android.app.Application
import android.content.Context
import com.kevicsalazar.appkit.scopes.PerApp
import com.kevicsalazar.gamarrapoint.ui.adapters.BinderAdapter
import com.kevicsalazar.gamarrapoint.utils.NetworkService
import com.kevicsalazar.gamarrapoint.utils.ResourcesProvider

import dagger.Module
import dagger.Provides

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@Module
class AppModule(private val app: App) {

    @Provides @PerApp fun provideContext(): Context = app

    @Provides @PerApp fun provideApplication(): Application = app

    @Provides @PerApp fun provideNetworkService(): NetworkService = NetworkService(app)

    @Provides @PerApp fun provideResourcesProvider(): ResourcesProvider = ResourcesProvider(app)

}
