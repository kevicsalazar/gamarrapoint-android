package com.kevicsalazar.gamarrapoint

import android.support.multidex.MultiDexApplication
import com.kevicsalazar.gamarrapoint.ui.mvp.model.ParseModel
import com.parse.Parse
import com.parse.ParseFacebookUtils

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
public open class App : MultiDexApplication() {

    val appComponent: AppComponent by lazy { AppComponent.Initializer.init(this) }

    override fun onCreate() {
        super.onCreate()
        setupComponent()
        ParseModel.register()
        Parse.enableLocalDatastore(this)
        Parse.initialize(this, BuildConfig.PARSE_APPLICATION_ID, BuildConfig.PARSE_CLIENT_KEY)
        ParseFacebookUtils.initialize(this)
    }

    fun setupComponent() = appComponent.inject(this)

}