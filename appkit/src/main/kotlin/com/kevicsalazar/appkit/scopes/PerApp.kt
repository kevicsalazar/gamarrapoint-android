package com.kevicsalazar.appkit.scopes

import javax.inject.Scope

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation public class PerApp
