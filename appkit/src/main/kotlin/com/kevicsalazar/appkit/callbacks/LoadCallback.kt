package com.kevicsalazar.appkit.callbacks

import com.kevicsalazar.appkit.enums.LoadStatus

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
class LoadCallback<L>(

        /**
         * Called when load status changes
         * and return [LoadStatus.LOADED]
         * *          [LoadStatus.LOADING]
         */
        val onLoadStatus: (LoadStatus) -> Unit,

        /**
         * Called when load finished successfully
         * and return the generic type
         */
        val onLoadSuccess: (L) -> Unit,

        /**
         * Called when load finished unsuccessfully
         * and return the string resId for error message
         */
        val onLoadFailure: (Int) -> Unit)
