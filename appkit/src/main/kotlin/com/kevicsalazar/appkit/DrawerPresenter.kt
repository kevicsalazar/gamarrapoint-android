package com.kevicsalazar.appkit

import android.support.v7.widget.Toolbar
import android.view.MenuItem

/**
 * Created by Kevin Salazar
 */
interface DrawerPresenter : BasePresenter {

    fun setupDrawerToggle(toolbar: Toolbar);

    fun openDrawerItemView(menuItem: MenuItem): Boolean

    fun closeDrawerMenu()

}
