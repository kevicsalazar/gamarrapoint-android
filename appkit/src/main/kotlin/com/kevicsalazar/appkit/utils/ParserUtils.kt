package com.kevicsalazar.appkit.utils

/**
 * Created by Kevin Salazar
 */
@Suppress("unchecked_cast")
object ParserUtils {

    fun <C> parse(list: List<Any>): MutableList<C>{
        var newList: MutableList<C> = arrayListOf()
        for (o in list) {
            newList.add(o as C)
        }
        return newList
    }

}
