package com.kevicsalazar.appkit.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Kevin Salazar
 */
object AppDateUtils {

    val F1 = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
    val F2 = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val F3 = SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault())
    val F4 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

    fun addSeconds(date: Date, second: Int): Date {
        val calendar = Calendar.getInstance()
        calendar.time = date
        calendar.add(Calendar.SECOND, second)
        return calendar.time
    }

}
