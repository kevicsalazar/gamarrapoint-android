package com.kevicsalazar.appkit.model

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
data class GuideModel(val titleId: Int, val descriptionId: Int, val colorId: Int, val screenshotId: Int)