package com.kevicsalazar.appkit.enums

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
enum class LoadStatus(val refreshing: Boolean) {

    LOCAL(false),
    LOADING(true),
    LOADED(false);

}
