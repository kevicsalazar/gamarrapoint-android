package com.kevicsalazar.appkit.enums

import com.kevicsalazar.appkit.utils.AppDateUtils
import java.util.*

/**
 * Created by Kevin Salazar
 */
enum class UpdateTime internal constructor(val time: Int) {

    NOW(0), // Ahora
    MIN(10), // 10 segundos
    AUTOHIGH(60 * 30), // 30 minutos
    AUTOMED(60 * 60 * 6), //  6 horas
    AUTOLOW(60 * 60 * 12); // 12 horas

    companion object {
        fun isTimeToUpdate(lasUpdatedTime: Long, time: UpdateTime): Boolean {
            return lasUpdatedTime.equals(0) || AppDateUtils.addSeconds(Date(lasUpdatedTime), time.time).time < Date().time
        }
    }

}
