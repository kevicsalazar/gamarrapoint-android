package com.kevicsalazar.appkit.enums

/**
 * Created by Kevin Salazar
 */
enum class Origin {
    
    DEFAULT,
    LOCAL,
    PARSE

}