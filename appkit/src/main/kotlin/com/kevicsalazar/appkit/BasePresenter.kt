package com.kevicsalazar.appkit

/**
 * Created by Kevin Salazar
 */
interface BasePresenter {

    /**
     * This method will be executed on
     * [AppCompatActivity.onStart] in case presenter is attached to activity
     * [Fragment.onStart]  in case presenter is attached to fragment
     */
    fun onStart()

    /**
     * This method will be executed on
     * [AppCompatActivity.onStop] in case presenter is attached to activity
     * [Fragment.onStop]  in case presenter is attached to fragment
     */
    fun onStop()

}
