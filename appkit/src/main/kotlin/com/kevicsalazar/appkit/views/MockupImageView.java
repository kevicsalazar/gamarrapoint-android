package com.kevicsalazar.appkit.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */
public class MockupImageView extends FrameLayout {

    private Paint deviceFill;
    private RectF deviceRect;
    private RectF buttonRect;

    private ImageView screenshot;

    public MockupImageView(Context context) {
        super(context);
        setWillNotDraw(false);
        init();
    }

    public MockupImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
        init();
    }

    private void init() {

        deviceRect = new RectF();
        buttonRect = new RectF();

        deviceFill = new Paint();
        deviceFill.setColor(Color.parseColor("#42000000"));
        deviceFill.setStyle(Paint.Style.FILL);

        setupScreenshot();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int deviceHeight = getMeasuredHeight();
        int deviceWidth = getMeasuredWidth();

        deviceRect.set(0, 0, deviceWidth, deviceHeight);
        canvas.drawRoundRect(deviceRect, 50f, 50f, deviceFill);

        buttonRect.set((deviceWidth / 2) - (deviceWidth / 8), deviceHeight - ((deviceWidth / 18) + (deviceWidth / 10)), (deviceWidth / 2) + (deviceWidth / 8), deviceHeight - (deviceWidth / 18));
        canvas.drawRoundRect(buttonRect, (deviceWidth / 20), (deviceWidth / 20), deviceFill);
    }

    private void setupScreenshot() {
        screenshot = new ImageView(getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins(25, 150, 25, 180);
        screenshot.setLayoutParams(layoutParams);
        addView(screenshot);
    }

    public void setScreenshot(int resId) {
        screenshot.setImageResource(resId);
    }

}
